function problem5(inventory, yearArr) {
    if (!Array.isArray(inventory) || inventory == undefined || yearArr == undefined || !Array.isArray(yearArr)) {
        return [];
    } else {
        let count = 0;
        let oldCarArr = [];
        for (let i = 0; i < yearArr.length; i++) {
            if (yearArr[i] < 2000) {
                count++;
                oldCarArr.push(inventory[i]);
            }
        }
        return { "count": count, "oldCarArr": oldCarArr };
    }
}

module.exports = problem5;