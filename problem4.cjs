function problem4(inventory) {
    if (!Array.isArray(inventory) || inventory == undefined) {
        return [];
    } else {
        let arr = [];
        for (let i = 0; i < inventory.length; i++) {
            arr.push(inventory[i].car_year);
        }
        return arr;
    }
}

module.exports = problem4;