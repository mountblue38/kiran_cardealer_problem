function problem3(inventory) {
    if (!Array.isArray(inventory) || inventory == undefined) {
        return [];
    } else {
        for (let i = 0; i < inventory.length; i++) {
            for (let j = i + 1; j < inventory.length; j++) {
                if (inventory[i].car_model > inventory[j].car_model) {
                    temp = inventory[i];
                    inventory[i] = inventory[j];
                    inventory[j] = temp;
                }
            }
        }
        return inventory;
    }
}

module.exports = problem3;


