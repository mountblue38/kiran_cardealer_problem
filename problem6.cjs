function problem6(inventory) {
    if (!Array.isArray(inventory) || inventory == undefined) {
        return [];
    } else {
        let BMWandAudi = [];
        for (let i = 0; i < inventory.length; i++) {
            if (inventory[i].car_make == "BMW" || inventory[i].car_make == "Audi") {
                BMWandAudi.push(inventory[i]);
            }
        }
        return JSON.stringify(BMWandAudi);
    }
}

module.exports = problem6;