function problem1(inventory, searchId) {
  if (!Array.isArray(inventory) || isNaN(searchId) || inventory == undefined || searchId == undefined ) {
    return [];
  } else {
    for (let i = 0; i < inventory.length; i++) {
      if (inventory[i].id == searchId) {
        return inventory[i];
      }
    }
    return [];
  }
}

module.exports = problem1;
