function problem2(inventory) {
    if (!Array.isArray(inventory) || inventory == undefined) {
        return [];
    } else {
        let lastcar = inventory.length - 1;
        return inventory[lastcar];
    }
}

module.exports = problem2;
