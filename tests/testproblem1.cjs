const inventory = require("../carsInventory.cjs");
const problem1 = require("../problem1.cjs");

const result = problem1(inventory,33);
console.log(`car ${result.id} is a ${result.car_year} ${result.car_make} ${result.car_model}`);