const inventory = require("../carsInventory.cjs");
const problem2 = require("../problem2.cjs");

const result = problem2(inventory);
console.log(`Last car is a ${result.car_make} ${result.car_model}`);